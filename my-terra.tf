# Configure the AWS Provider
provider "aws" {
  region                  = "ap-southeast-1"
}
resource "aws_instance" "web" {
  ami               = "ami-04677bdaa3c2b6e24"
  instance_type     = "t2.micro"
  user_data         = "${file("deploy.sh")}"
  vpc_security_group_ids = ["${aws_security_group.allow_all.id}"]
}

resource "aws_security_group" "allow_all" {
  name        = "allow_all"
  description = "Allow all inbound traffic"

  ingress {
    from_port   = 8000
    to_port     = 8000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_all"
  }
}

output "ip" {
  value = "${aws_instance.web.public_ip}"
}
output "server_id" {
  value = "${aws_instance.web.id}"
}
